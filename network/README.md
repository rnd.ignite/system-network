# Các cách triển khai network

##  Cách 1

###   Bước 1. Tạo cấu hình của network

./generate.sh

###   Bước 2. Triển khai network

./start.sh

###   Bước 3. Dừng triển khai network

./stop.sh

##  Cách 2. Nếu cách 1 lỗi :)

###   Bước 1. Comment lại dòng thứ 21 của start.sh

###   Bước 2. Tạo cấu hình network

./generate.sh

###   Bước 3. Triển khai network

./start.sh

###   Bước 4. Chạy container cli sau đó thực hiện thử công các bước bên dưới trong container cli.

docker exec -it cli bash

####     Step 4.1. Create Channel

export CHANNEL_NAME=organchannel
peer channel create -o orderer.organ.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx

####     Step 4.2. join peer0.hospital.organ.com to the channel

peer channel join -b organchannel.block

####     Step 4.3. join peer0.patient.organ.com to the channel

CORE_PEER_ADDRESS=peer0.patient.organ.com:7051 CORE_PEER_LOCALMSPID=PatientMSP CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/patient.organ.com/users/Admin@patient.organ.com/msp CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/patient.organ.com/peers/peer0.patient.organ.com/tls/ca.crt peer channel join -b organchannel.block

####     Step 4.4. Define the anchor peer for Hospital as peer0.hospital.organ.com

CORE_PEER_ADDRESS=peer0.hospital.organ.com:7051 CORE_PEER_LOCALMSPID=HospitalMSP CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hospital.organ.com/users/Admin@hospital.organ.com/msp CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hospital.organ.com/peers/peer0.hospital.organ.com/tls/ca.crt peer channel update -o orderer.organ.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/HospitalMSPanchors.tx

####     Step 4.5. Define the anchor peer for Patient as peer0.patient.organ.com

CORE_PEER_ADDRESS=peer0.patient.organ.com:8051 CORE_PEER_LOCALMSPID=PatientMSP CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/patient.organ.com/users/Admin@patient.organ.com/msp CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/patient.organ.com/peers/peer0.patient.organ.com/tls/ca.crt peer channel update -o orderer.organ.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/PatientMSPanchors.tx

####     Step 4.6. Install the chaincode onto the peer0 node in Hospital

CORE_PEER_ADDRESS=peer0.hospital.organ.com:7051 CORE_PEER_LOCALMSPID=HospitalMSP CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hospital.organ.com/users/Admin@hospital.organ.com/msp CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hospital.organ.com/peers/peer0.hospital.organ.com/tls/ca.crt peer chaincode install -n hospital -v 1.0 -p github.com/chaincode/hospital/go/

####     Step 4.7. Instantiate the chaincode onto the peer0 node in Hospital

CORE_PEER_ADDRESS=peer0.hospital.organ.com:7051 CORE_PEER_LOCALMSPID=HospitalMSP CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hospital.organ.com/users/Admin@hospital.organ.com/msp CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/hospital.organ.com/peers/peer0.hospital.organ.com/tls/ca.crt peer chaincode instantiate -o orderer.organ.com:7050 -C $CHANNEL_NAME -n hospital -v 1.0 -c '{"Args":[]}' -P "OR ('HospitalMSP.peer','PatientMSP.peer')"

####     Step 4.8. Install the chaincode onto the peer0 node in Patient

CORE_PEER_ADDRESS=peer0.patient.organ.com:7051 CORE_PEER_LOCALMSPID=PatientMSP CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/patient.organ.com/users/Admin@patient.organ.com/msp CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/patient.organ.com/peers/peer0.patient.organ.com/tls/ca.crt peer chaincode install -n hospital -v 1.0 -p github.com/chaincode/hospital/go/

####     Step 4.9. Query Chaincode


###   Bước 5. Dừng triển khai network

exit

./stop.sh
