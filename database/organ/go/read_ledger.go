package main

import (
	"encoding/json"
	"errors"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func getHospital(stub shim.ChaincodeStubInterface, compoundKey string) (Hospital, error) {

	var hospital Hospital

	hospitalAsBytes, err := stub.GetState(compoundKey)

	if err != nil {
		return hospital, errors.New("Failed to get hospital - " + compoundKey)
	}

	if hospitalAsBytes == nil {
		return hospital, errors.New("Hospital does not exist - " + compoundKey)
	}

	json.Unmarshal(hospitalAsBytes, &hospital)

	return hospital, nil
}

func getDonation(stub shim.ChaincodeStubInterface, compoundKey string) (Donation, error) {

	var donation Donation

	donationAsBytes, err := stub.GetState(compoundKey)

	if err != nil {
		return donation, errors.New("Failed to get donation - " + compoundKey)
	}

	if donationAsBytes == nil {
		return donation, errors.New("Donation does not exist - " + compoundKey)
	}

	json.Unmarshal(donationAsBytes, &donation)

	return donation, nil
}

func getQueue(stub shim.ChaincodeStubInterface, compoundKey string) (Queue, error) {

	var queue Queue

	queueAsBytes, err := stub.GetState(compoundKey)

	if err != nil {
		return queue, errors.New("Failed to get queue - " + compoundKey)
	}

	if queueAsBytes == nil {
		return queue, errors.New("Queue does not exist - " + compoundKey)
	}

	json.Unmarshal(queueAsBytes, &queue)

	return queue, nil
}

func getListHospitals(stub shim.ChaincodeStubInterface) (shim.StateQueryIteratorInterface, error) {

	startKey := "Hospital-"
	endKey := "Hospital-zzzzzzzz"

	resultIter, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return nil, err
	}

	return resultIter, nil
}

func getListDonations(stub shim.ChaincodeStubInterface) (shim.StateQueryIteratorInterface, error) {

	startKey := "Donation-"
	endKey := "Hospital-zzzzzzzz"

	resultIter, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return nil, err
	}

	return resultIter, nil
}

func getListQueues(stub shim.ChaincodeStubInterface) (shim.StateQueryIteratorInterface, error) {

	startKey := "Queue-"
	endKey := "Queue-zzzzzzzz"

	resultIter, err := stub.GetStateByRange(startKey, endKey)
	if err != nil {
		return nil, err
	}

	return resultIter, nil
}