package main

import (
	"encoding/json"

	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

func CreateDonation(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	fmt.Println("Start Create Donation!")

	IdNumber := args[0]
	Organ := args[1]
	Date := args[2]
	Blood := args[3]
	Status := args[4]
	SurgeryDate := ""
	HospitalID := ""

	key := "Donation-" + " " + "Donor" + IdNumber + "Organ" + Organ
	_, err := getDonation(stub, key)

	if err == nil {
		return shim.Error("This donation already exists.")
	}

	var donation = Donation{IdNumber: IdNumber, Organ: Organ, Date: Date, Blood: Blood, Status: Status,SurgeryDate: SurgeryDate, HospitalID: HospitalID}

	donationAsBytes, _ := json.Marshal(donation)

	stub.PutState(key, donationAsBytes)

	return shim.Success(nil)
}

func CreateQueue(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	fmt.Println("Start Create Queue!")

	IdNumber := args[0]
	Organ := args[1]
	Date := args[2]
	Blood := args[3]
	Status := args[4]
	TransplantationDate := ""
	HospitalID := ""
	DonorIDNumber := ""

	key := "Queue-" + " " + "Receiver" + IdNumber + "Organ" + Organ
	_, err := getQueue(stub, key)

	if err == nil {
		return shim.Error("This queue already exists.")
	}

	var queue = Queue{IdNumber: IdNumber, Organ: Organ, Date: Date, Blood: Blood, Status: Status, TransplantationDate: TransplantationDate, HospitalID: HospitalID, DonorIDNumber: DonorIDNumber}

	queueAsBytes, _ := json.Marshal(queue)

	stub.PutState(key, queueAsBytes)

	return shim.Success(nil)
}

func CreateHospital(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	fmt.Println("Start Create Student!")

	HospitalID := args[0]
	Name := args[1]
	Address := args[2]

	key := "Hospital-" + HospitalID
	_, err := getHospital(stub, key)

	if err == nil {
		return shim.Error("This hospital already exists - " + HospitalID)
	}

	var hospital = Hospital{HospitalID: HospitalID, Name: Name, Address: Address}

	hospitalAsBytes, _ := json.Marshal(hospital)

	stub.PutState(key, hospitalAsBytes)

	return shim.Success(nil)
}

func Surgery(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	IdNumber := args[0]
	Organ := args[1]
	Status := args[2]
	HospitalID := args[3]
	SurgeryDate := args[4]

	key := "Donation-" + " " + "Donor" + IdNumber + "Organ" + Organ
	donation, err := getDonation(stub, key)

	if err == nil {
		return shim.Error("This donation already exists.")
	}
	donation.Status = Status
	donation.HospitalID = HospitalID
	donation.SurgeryDate = SurgeryDate
	donationAsBytes, _ := json.Marshal(donation)
	stub.PutState(key, donationAsBytes)
	return shim.Success(nil)
}

func Transplantation(stub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	IdNumber := args[0]
	Organ := args[1]
	Status := args[2]
	HospitalID := args[3]
	TransplantationDate := args[4]
	DonorIDNumber := args[5]

	key := "Queue-" + " " + "Receiver" + IdNumber + "Organ" + Organ
	queue, err := getQueue(stub, key)

	if err == nil {
		return shim.Error("This queue already exists.")
	}
	queue.Status = Status
	queue.HospitalID = HospitalID
	queue.TransplantationDate = TransplantationDate
	queue.DonorIDNumber = DonorIDNumber
	queueAsBytes, _ := json.Marshal(queue)

	stub.PutState(key, queueAsBytes)
	return shim.Success(nil)
}

func main() {

	err := shim.Start(new(SmartContract))

	if err != nil {
		fmt.Printf("Error createing new Smart Contract: %s", err)
	}
}
