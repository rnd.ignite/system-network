package main

import (
	"encoding/json"
	// "fmt"

	// "github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

type SmartContract struct {
}

type Hospital struct {
	HospitalID string
	Name string
	Address string
}

type Queue struct {
	IdNumber string
	Organ string
	Date string
	Blood string
	Status string
	TransplantationDate string
	HospitalID string
	DonorIDNumber string
}

type Donation struct {
	IdNumber string
	Organ string
	Date string
	Blood string
	Status string
	SurgeryDate string
	HospitalID string
}

func (s *SmartContract) Init(stub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (s *SmartContract) Invoke(stub shim.ChaincodeStubInterface) sc.Response {

	function, args := stub.GetFunctionAndParameters()

	if function == "CreateHospital" {
		return CreateHospital(stub, args)
	} else if function == "CreateQueue" {
		return CreateQueue(stub, args)
	} else if function == "CreateDonation" {
		return CreateDonation(stub, args)
	} else if function == "QueryHospital" {
		return QueryHospital(stub, args)
	} else if function == "QueryQueue" {
		return QueryQueue(stub, args)
	} else if function == "QueryDonation" {
		return QueryDonation(stub, args)
	} else if function == "GetAllHospitals" {
		return GetAllHospitals(stub)
	} else if function == "GetAllQueues" {
		return GetAllQueues(stub)
	} else if function == "GetAllDonations" {
		return GetAllDonations(stub)
	}

	return shim.Error("Invalid Smart Contract function name!")
}

func QueryHospital(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	HospitalID := args[0]

	key := "Hospital-" + HospitalID
	hospitalAsBytes, err := stub.GetState(key)

	if err != nil {
		return shim.Error("Failed")
	}

	if hospitalAsBytes == nil {

		return shim.Error("Hospital does not exits - " + HospitalID)

	} else {

		return shim.Success(hospitalAsBytes)

	}
}


func QueryDonation(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	DonorID := args[0]
	Organ := args[1]

	key := "Donation-" + " " + "Donor" + DonorID + "Organ" + Organ
	donationAsBytes, err := stub.GetState(key)

	if err != nil {
		return shim.Error("Failed")
	}

	if donationAsBytes == nil {

		return shim.Error("Donation does not exits - ")

	} else {

		return shim.Success(donationAsBytes)

	}
}

func QueryQueue(stub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	ReceiverID := args[0]
	Organ := args[1]

	key := "Receiver-" + " " + "Donor" + ReceiverID + "Organ" + Organ
	receiverAsBytes, err := stub.GetState(key)

	if err != nil {
		return shim.Error("Failed")
	}

	if receiverAsBytes == nil {

		return shim.Error("Receiver does not exits - ")

	} else {

		return shim.Success(receiverAsBytes)

	}
}

func GetAllHospitals(stub shim.ChaincodeStubInterface) sc.Response {

	allHospitals, _ := getListHospitals(stub)

	defer allHospitals.Close()

	var tlist []Hospital
	var i int

	for i = 0; allHospitals.HasNext(); i++ {

		record, err := allHospitals.Next()

		if err != nil {
			return shim.Success(nil)
		}

		hospital := Hospital{}
		json.Unmarshal(record.Value, &hospital)
		tlist = append(tlist, hospital)
	}

	jsonRow, err := json.Marshal(tlist)

	if err != nil {
		return shim.Error("Failed")
	}

	return shim.Success(jsonRow)
}

func GetAllDonations(stub shim.ChaincodeStubInterface) sc.Response {

	allDonations, _ := getListDonations(stub)

	defer allDonations.Close()

	var tlist []Donation
	var i int

	for i = 0; allDonations.HasNext(); i++ {

		record, err := allDonations.Next()

		if err != nil {
			return shim.Success(nil)
		}

		donation := Donation{}
		json.Unmarshal(record.Value, &donation)
		tlist = append(tlist, donation)
	}

	jsonRow, err := json.Marshal(tlist)

	if err != nil {
		return shim.Error("Failed")
	}

	return shim.Success(jsonRow)
}

func GetAllQueues(stub shim.ChaincodeStubInterface) sc.Response {

	allQueues, _ := getListQueues(stub)

	defer allQueues.Close()

	var tlist []Queue
	var i int

	for i = 0; allQueues.HasNext(); i++ {

		record, err := allQueues.Next()

		if err != nil {
			return shim.Success(nil)
		}

		queue := Queue{}
		json.Unmarshal(record.Value, &queue)
		tlist = append(tlist, queue)
	}

	jsonRow, err := json.Marshal(tlist)

	if err != nil {
		return shim.Error("Failed")
	}

	return shim.Success(jsonRow)
}
